import 'package:ealmashi/core/utils/light_theme_colors.dart';
import 'package:ealmashi/features/Screen/home/bloc/pages_state.dart';
import 'package:ealmashi/features/Screen/login/mainlog/mainlogscreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'dart:ui' as ui;

import 'package:flutter_svg/flutter_svg.dart';

import '../../../Globals.dart';
import 'bloc/PagesBloc.dart';

class Chat extends StatefulWidget {
  VoidCallback? voidCallback;
  String? activity_name;
  String? user_name;
  int? user_id;
  String? activity_phone;
  int? activity_id;
  int? conversation_id;
  PagesBloc? bloc;
 int? index;
  Chat(
      {Key? key,
      this.voidCallback,
      this.activity_name,
      this.activity_id,
      this.bloc,
        this.activity_phone,
        this.index,
        this.user_name,
      this.conversation_id,
      this.user_id})
      : super(key: key);

  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {

  TextEditingController content=TextEditingController();
  @override
  void initState() {

    widget.bloc!.onGetConvEvent(widget.conversation_id!);
    content.clear();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Global.userToken!.length > 2 ? BlocConsumer<PagesBloc, PagesState>(
                     bloc: widget.bloc,
                  listener: (context, state) {
                    // TODO: implement listener
                  },
                  builder: (context, state) {

                    if(state.isLoadingChat!)
                      return SizedBox(
                          height: 0.9.sh,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 20.h,
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20.w),
                                child: Row(
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: Container(
                                          height: 46.h,
                                          width: 84.w,
                                          child: Image.asset(
                                            'assets/images/logo.png',
                                            fit: BoxFit.fill,
                                          )),
                                    ),
                                    SizedBox(
                                      width: 20.w,
                                    ),
                                    Expanded(
                                      flex: 2,
                                      child: Container(
                                        width: 0.6.sw,
                                        height: 46.h,
                                        decoration: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    width: 2,
                                                    color: Colors.grey.shade400))),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: [
                                            InkWell(
                                                onTap: () {},
                                                child: Icon(
                                                  Icons.arrow_back_ios,
                                                  textDirection: ui.TextDirection.ltr,
                                                ))
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 0.6.sh,
                                child: Center(
                                  child: CircularProgressIndicator(),
                                ),
                              ),
                            ],
                          ));
                    return Stack(
                        children: [
                      SizedBox(
                        height: 0.85.sh,
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              SizedBox(
                                height: 25.h,
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20.w),
                                child: Row(
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: Container(
                                          height: 46.h,
                                          width: 84.w,
                                          child: Image.asset(
                                            'assets/images/logo.png',
                                            fit: BoxFit.fill,
                                          )),
                                    ),
                                    SizedBox(
                                      width: 20.w,
                                    ),
                                    Expanded(
                                      flex: 2,
                                      child: Container(
                                        width: 0.6.sw,
                                        height: 46.h,
                                        decoration: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    width: 2,
                                                    color: Colors.grey.shade400))),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: [
                                            InkWell(
                                                onTap: () {
                                                  Navigator.pop(context);
                                                },
                                                child: Icon(
                                                  Icons.arrow_back_ios,
                                                  textDirection:
                                                      ui.TextDirection.ltr,
                                                ))
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 10.h,
                              ),
                              SizedBox(
                                height: 20.h,
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20.w),
                                child: SizedBox(
                                  height: 47.h,
                                  width: 1.sw,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: LightThemeColors.secondColor,
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    child: Padding(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 10.w),
                                      child: Row(
                                        children: [
                                          Text(
                                            widget.activity_name!,
                                            style: TextStyle(
                                              fontSize: 15.sp,
                                              color: Colors.white,
                                              fontFamily: 'Noto',
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),

                              if(widget.activity_phone!=null)
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    vertical: 5.0.h, horizontal: 28.w),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 5.0.h, horizontal: 4.w),
                                        child: Text(
                                          widget.activity_phone!,
                                          style: TextStyle(
                                              fontSize: 14.sp,
                                              color: LightThemeColors.secondColor,
                                              fontFamily: "Noto",
                                              fontWeight: FontWeight.w500),
                                          textAlign: TextAlign.start,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                    ),
                                    SvgPicture.asset(
                                        'assets/icons/FontAwsome (phone-square-alt).svg'),
                                  ],
                                ),
                              ),

                              for(int i=0;i<state.getChatModel!.data!.length;i++)
                              Column(children: [
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 5.0.h, horizontal: 2.w),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        height: 40.h,
                                        width: 0.9.sw,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(25),
                                          border: Border.all(
                                            color: state.getChatModel!.data![i].is_from_user==1? LightThemeColors
                                                .secondColor:LightThemeColors
                                                .lightYellowColor,
                                          ),
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                children: [
                                                  Padding(
                                                    padding:
                                                    const EdgeInsets.all(2.0),
                                                    child: SvgPicture.asset(
                                                      'assets/icons/FontAwsome (comment-alt).svg',
                                                      color: state.getChatModel!.data![i].is_from_user==1? LightThemeColors
                                                          .secondColor:LightThemeColors
                                                          .lightYellowColor,
                                                    ),
                                                  ),
                                                  Text(state.getChatModel!.data![i].is_from_user==1?widget.user_name!:widget.activity_name!,
                                                    style: TextStyle(
                                                        fontSize: 14.sp,
                                                          color: state.getChatModel!.data![i].is_from_user==1? LightThemeColors
                                                            .secondColor:LightThemeColors
                                                              .lightYellowColor,
                                                        fontFamily: "Noto",
                                                        fontWeight: FontWeight.w500),
                                                    textAlign: TextAlign.start,
                                                    overflow: TextOverflow.ellipsis,
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                MainAxisAlignment.end,
                                                children: [
                                                  Text(state.getChatModel!.data![i].created_at!.substring(0,10),
                                                    style: TextStyle(
                                                        fontSize: 12.sp,
                                                        color: state.getChatModel!.data![i].is_from_user==1? LightThemeColors
                                                            .secondColor:LightThemeColors
                                                            .lightYellowColor,
                                                        fontFamily: "Noto",
                                                        fontWeight: FontWeight.w500),
                                                    textAlign: TextAlign.start,
                                                    overflow: TextOverflow.ellipsis,
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 5.0.h, horizontal: 20.w),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Expanded(
                                              child: Row(
                                                children: [
                                                  Text(state.getChatModel!.data![i].content!,
                                                    style: TextStyle(
                                                        fontSize: 12.sp,
                                                        color: state.getChatModel!.data![i].is_from_user==1? LightThemeColors
                                                            .secondColor:LightThemeColors
                                                            .lightYellowColor,
                                                        fontFamily: "Noto",
                                                        fontWeight: FontWeight.w500),
                                                    textAlign: TextAlign.start,
                                                    overflow: TextOverflow.ellipsis,
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Expanded(
                                              child: Row(
                                                mainAxisAlignment:
                                                MainAxisAlignment.end,
                                                children: [
                                                  Icon(Icons.check),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],),

                            ],
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Column(
                          children: [
                            SizedBox(
                              height: 0.86.sh,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 5.0.h, horizontal: 2.w),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    height: 40.h,
                                    width: 0.9.sw,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      border: Border.all(
                                        color:
                                            LightThemeColors.lightYellowColor,
                                      ),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            flex: 3,
                                            child: TextField(
                                              controller: content,
                                               onChanged: (va){
                                                setState(() {

                                                });
                                               },
                                              style: TextStyle(
                                                  fontSize: 12.sp,
                                                  color: Colors.black),
                                              decoration: new InputDecoration(
                                                hintText: 'اكتب الرسالة هنا...',
                                                hintStyle: TextStyle(
                                                    fontSize: 13.sp,
                                                    height: 0.7.h,
                                                    color: LightThemeColors
                                                        .lightGreyShade400),
                                                border: InputBorder.none,
                                              ),
                                            ),
                                          ),
                                          InkWell(
                                            onTap: content.text.length>0?(){

                                              if(Global.userID==widget.user_id)
                                                widget.bloc!.onSendMassegEvent(content.text, widget.conversation_id!, 1);
                                              else
                                                widget.bloc!.onSendMassegEvent(content.text, widget.conversation_id!, 0);

                                              widget.bloc!.onGetConvEvent(widget.conversation_id!);
                                              content.text='';
                                            }:null,
                                            child: SizedBox(
                                              child: SvgPicture.asset(
                                                'assets/icons/FontAwsome (paper-plane).svg',
                                                color: LightThemeColors
                                                    .lightYellowColor,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ]);
                  },
                )
              : SizedBox(
                  height: 0.9.sh,
                  width: 1.sw,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 5.h,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Color(0xffEAA947),
                            ),
                            borderRadius: BorderRadius.circular(7)),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 8.0.h, horizontal: 4.w),
                          child: SizedBox(
                            width: 0.5.sw,
                            child: Text(
                              'يجب  تسجيل  الدخول',
                              style: TextStyle(
                                  fontSize: 13.sp,
                                  color: LightThemeColors.primaryColor,
                                  fontFamily: "Nahdi",
                                  fontWeight: FontWeight.w900),
                              textAlign: TextAlign.center,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 5.h,
                      ),
                      MaterialButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        color: LightThemeColors.darkBackgroundColor,
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => MainLog()),
                          );
                        },
                        child: SizedBox(
                          width: 0.5.sw,
                          child: Text(
                            'العودة  لتسجيل  الدخول',
                            style: TextStyle(
                                fontSize: 16.sp,
                                color: LightThemeColors.backgroundColor,
                                fontFamily: "Nahdi",
                                fontWeight: FontWeight.w900),
                            textAlign: TextAlign.center,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
        ),
      ),
    );
  }
}
